
var pool = require(__dirname+'/database/sqlmiddleware.js');


module.exports = function(app, passport,io,board,ns) {

  app.use(function (req, res, next) {
      console.log(req.body);
      
    next()
    });
//Home
 app.get('/', function(req, res) {
        res.render('index.ejs'); // 
    });
    
//Login     
  app.get('/login', function(req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });
    
  app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/listboard',
        failureRedirect : '/login', 
        failureFlash : true
        
    }));
//Register
  app.get('/register', function(req, res) {
        res.render('register.ejs', { message: req.flash('signupMessage') });
    });
  app.post('/register', passport.authenticate('local-signup', {
        successRedirect : '/login', 
        failureRedirect : '/register', 
        failureFlash : true 
        
    }));
//ListBoard
  app.get('/listboard',(req,res)=>{
    if (req.isAuthenticated()){
      setImmediate(function(){
        res.render('listboard.ejs',{ useraut:req.user.email});
      });
      io.of('/listboard').on('connect', function(socket){
        console.log('someone connected from: ' + socket.handshake.address);
        
        socket.on('select board',function(i){
          console.log(socket.handshake.address + ': chon board '+(i+1));
          board[i].status = "Connected";
          board[i].user=req.user.email;
          board[i].x=0;
          board[i].y=0;
          board[i].fire=false;
          io.of('/listboard').emit('listboard', board);
      
        });
        socket.on('disconnect board',function(i){
          console.log(socket.handshake.address + ': left board '+(i+1));
          board[i].status = "Ready to connect";
          board[i].user="";
          board[i].x=0;
          board[i].y=0;
          board[i].fire=false;
          io.of('/listboard').emit('listboard', board);
        });
      });            
    }
    else{
      res.redirect('/login');
    }  
  });
  
//Find enemy  
  app.get('/findenemy',(req,res)=>{
    if (req.isAuthenticated()){
    res.render('findenemy',{message: req.flash('checkenemy')});

    }
    else{
      res.redirect('/login');
    }
  });
  app.post('/findenemy',async function(req,res){
    if (req.isAuthenticated()){
      let enemyname=req.body.enemy;
      let myname=req.user.email;
      console.log(enemyname);
      console.log(myname);  
      if (enemyname==myname){
        req.flash('checkenemy','Can\'t play with yourself');
        return res.redirect('/findenemy');
      }
      else{
        for(var i=0;i<board.length;i++){
          if (enemyname==board[i].user){
            req.flash('checkenemy',' OK.Press Next to begin the battle ');
            var sqlquery='UPDATE users SET enemy=\"'+enemyname+'\" WHERE email=\"'+myname+'\"';
            var info= await pool.query(sqlquery);
            console.log(info);
            return res.redirect('/refresh');
          }
        }
      req.flash('checkenemy','Can\'t find your enemy ');    
      return res.redirect('/findenemy');  
      }
    }
    else{
      res.redirect('/login');
    }
  });
//Refresh data
 app.get('/refresh',async function(req, res, next){
    if (req.isAuthenticated()){
      try{
        var sqlquery;
        var rand;
        var name=req.user.email;
        sqlquery='delete from mymap where email=\"'+name+'\"'; 
        await pool.query(sqlquery);
      
        var id_b;
        for (var i=0;i<8;i++){
          for(var j=0;j<8;j++){
            id_b=i*10+j;
            sqlquery='INSERT INTO mymap ( email,id_b,stt) values (\"'+name+'\",'+id_b+',0)';
            sql= await pool.query(sqlquery);
          }
        }
        for (var i=0;i<8;i++){
          rand =getRandomInt(0,7)+i*10;
          sqlquery='update mymap set stt=1 where id_b='+rand+' and email=\"'+name+'\"';
          var sql= await pool.query(sqlquery);
          //console.log(sql);
        }
        sqlquery='update users set turn=true where email=\"'+name+'\"';
        await pool.query(sqlquery);
        sqlquery='select * from users where email=\"'+name+'\"';
        var result =await pool.query(sqlquery);
        sqlquery='update users set turn=false where email=\"'+result[0].enemy+'\"';
        await pool.query(sqlquery);
        return res.redirect('/findenemy')
      }
      catch(err){
          throw err
      }
    }
    else{
       res.redirect('/login');
    }  
 });
//Play
  app.get('/play',function(req,res,next){
    if (req.isAuthenticated()){
      var index=findIndexUser(board,req.user.email);
     
      if (index != -1){
        res.render('play.ejs',{ useraut:req.user.email,indexaut:index});
      }
      else{
        res.redirect('/listboard');
      }
      
      io.of('/play').on('connect', function(socket){
        console.log('Play from: ' + socket.id);
        var temp1;
        var countdown =60;
        socket.on('get user',async function(temp){

          var index =findIndexUser(board,temp);
          if(index==-1) return 0;
          try{
           var sqlquery='select * from users where email=\"'+temp+'\"';
           var row= await pool.query(sqlquery);
           sqlquery='select * from mymap where email=\"'+temp+'\"';
           var map = await pool.query(sqlquery);
           var map_ta=new Array(8);
           for (var i=0;i<map_ta.length;i++){
             map_ta[i]=new Array(8);
           }
           for(var i=0;i<map.length;i++){
               var y=(map[i].id_b)%10;
               var x=((map[i].id_b)-y)/10;
               if (map[i].stt==1){map_ta[x][y]="A";}
               if (map[i].stt==2){map_ta[x][y]="S";}
               if (map[i].stt==3){map_ta[x][y]="B";}
           }
           var map_dich=new Array(8);
           for (var i=0;i<map_dich.length;i++){
             map_dich[i]=new Array(8);
           }
           sqlquery='select * from mymap where email=\"'+row[0].enemy+'\"';
           map = await pool.query(sqlquery);  
           for(var i=0;i<map.length;i++){
               var y=(map[i].id_b)%10;
               var x=((map[i].id_b)-y)/10;
               if (map[i].stt==2){map_dich[x][y]="S";}
               if (map[i].stt==3){map_dich[x][y]="B";}
           }
         
           var index_dich=findIndexUser(board,row[0].enemy);
           socket.emit('update tadich',map_ta,map_dich,board,index,index_dich,row[0].turn,countdown);
           temp1=temp;
          }
          catch(err){
            throw err;
          }
        });
        socket.on('user fire',async function(index){
          try{
           console.log('user fire')
           board[index].fire=false;
           var sqlquery='select * from users where email=\"'+temp1+'\"';
           var row= await pool.query(sqlquery);
           
           if(row[0].turn==true){
             sqlquery='select * from mymap where email=\"'+row[0].enemy+'\"';
             var map = await pool.query(sqlquery);
             var sql;
             var position=board[index].x * 10 + board[index].y;
             
             for (var i=0;i<map.length;i++){
               if (map[i].id_b==position) {
                 if (map[i].stt==0){
                   sqlquery='update mymap set stt=2 where email=\"'+row[0].enemy+'\" and id_b='+position;
                   sql=await pool.query(sqlquery);
                 }
                 else if(map[i].stt==1){
                   sqlquery='update mymap set stt=3 where email=\"'+row[0].enemy+'\" and id_b='+position;
                   sql=await pool.query(sqlquery);
                 }
                 break;
               }
             }  
             var map_ta=new Array(8);
             for(var i=0;i<map_ta.length;i++){
               map_ta[i]=new Array(8);
             }
             
             for(var i=0;i<map.length;i++){
               var y=(map[i].id_b)%10;
               var x=((map[i].id_b)-y)/10;
               if (map[i].stt==1){map_ta[x][y]="A";}
               if (map[i].stt==2){map_ta[x][y]="S";}
               if (map[i].stt==3){map_ta[x][y]="B";}
             }
             
             var map_dich=new Array(8);
             for (var i=0;i<map_dich.length;i++){
               map_dich[i]=new Array(8);
             }
             sqlquery='select * from mymap where email=\"'+row[0].enemy+'\"';
             map = await pool.query(sqlquery);  
             for(var i=0;i<map.length;i++){
               var y=(map[i].id_b)%10;
               var x=((map[i].id_b)-y)/10;
               if (map[i].stt==2){map_dich[x][y]="S";}
               if (map[i].stt==3){map_dich[x][y]="B";}
             }
             sqlquery='update users set turn=false where email=\"'+temp1+'\"';
             var update=await pool.query(sqlquery); 
             sqlquery='update users set turn=true where email=\"'+row[0].enemy+'\"';
             update=await pool.query(sqlquery); 
             var index_dich=findIndexUser(board,row[0].enemy);
             
             socket.emit('update tadich',map_ta,map_dich,board,index,index_dich,false,countdown);          
           }
          }
		  catch(err){
             throw err;
           }
        });
        socket.on('update countdown',function(temp){
          countdown = temp;
        });
        socket.on('disconnect', function(err){
         
         console.log('someone disconnect : ' + socket.id);
       });
              
      });

    }
    else{
      res.redirect('/login');
    }  
  
  });

};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function findIndexUser(board,user)
{
  for(var i = 0;i < board.length;i++){
    if(board[i].user == user)
      return i;
  }
  return -1;
}
