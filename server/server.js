const express =require("express");
const bodyParser=require("body-parser");
const session =require('express-session');
const passport =require('passport');

const app =express();
const morgan = require('morgan');
const flash  = require('connect-flash');
const server = require("http").Server(app);

const net = require('net');
const ns = net.createServer();
const io=require('socket.io')(server);

var board=[]; 

var match=[];

//=======================================================
app.set('views', __dirname + '/views'); 
app.set('view engine','ejs');

require(__dirname+'/passport.js')(passport);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({secret:"iloveyou"}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());  
app.use( express.static("public"));

//===========================================================
require(__dirname+'/board.js')(ns,io,board);
require(__dirname+'/routes.js')(app,passport,io,board,ns);

//=========================================================
ns.listen(8444,()=>console.log('connect board on port 8444'));
server.listen(8500,()=> console.log('web on port 8500'));
