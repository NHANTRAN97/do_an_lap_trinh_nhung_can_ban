	/****************************************************************************
	 * @file     main.c
	 * @version  V3.00
	 * $Revision: 10 $
	 * $Date: 15/06/04 1:56p $
	 * @brief
	 *           Transmit and receive data in UART RS485 mode.
	 *           This sample code needs to work with UART_RS485_Master.
	 * @note
	 * Copyright (C) 2011 Nuvoton Technology Corp. All rights reserved.
	 *
	 ******************************************************************************/
#include <stdio.h>
#include "NUC131.h"
#include <string.h>


	/*---------------------------------------------------------------------------------------------------------*/
	/* Define functions prototype                                                                              */
	/*---------------------------------------------------------------------------------------------------------*/

#define IS_USE_RS485NMM   0      //1:Select NMM_Mode , 0:Select AAD_Mode
#define MATCH_ADDRSS1     0xC0
#define MATCH_ADDRSS2     0xA2
#define UNMATCH_ADDRSS1   0xB1
#define UNMATCH_ADDRSS2   0xD3
#define PLL_CLOCK  			  50000000

	
extern char GetChar(void);
void RS485_HANDLE(void);
void A351_HANDLE(void);
void sendATcmd(char* ATcmd);
	

volatile int32_t send_wait = TRUE;
volatile int32_t send_ok = TRUE;
volatile int32_t g_check=TRUE;
char* ok ="OK\r\n";
	
int len_ok = 4;
int char_oki =0;
int i = 0;
char* send_string;
int len = 0;
	
	/*---------------------------------------------------------------------------------------------------------*/
	/* ISR to handle UART Channel 1 interrupt event                                                            */
	/*---------------------------------------------------------------------------------------------------------*/
	
	
	




void sendATcmd(char* ATcmd){
		i = 0;
		send_string = ATcmd;
		len = strlen(ATcmd);
		send_wait = TRUE;
		UART_EnableInt(UART3, UART_IER_THRE_IEN_Msk);
		while (send_wait);
		
	}

void A351_HANDLE(){
		char u8InChar = 0xFF;
		uint32_t u32IntSts = UART3->ISR;
			
		if(u32IntSts & UART_ISR_RDA_INT_Msk)
		{

			/* Get all the input characters */
			while(UART_IS_RX_READY(UART3))
			{
				/* Get the character from UART Buffer */
				u8InChar = UART_READ(UART3);

							printf("%c",u8InChar);
							if(send_ok)
							{
								if(u8InChar == ok[char_oki])
								{
									char_oki++;
									if(char_oki == len_ok)
									{
										send_ok = FALSE;
									}
								}
								else
										char_oki=0;
							}
	
				/* Check if buffer full */
				
			}
			
		}

		if(u32IntSts & UART_ISR_THRE_INT_Msk)
		{
			
				if(i>=len-1)
							{
								send_wait = FALSE;
								UART_DISABLE_INT(UART3, 0x00000002); 
							}
				while(UART_IS_TX_FULL(UART3));  // Wait Tx is not full to transmit data                           
				
							UART3->THR = send_string[i++];
			   
		 }
	}
		
	
void UART3_IRQHandler(void){
			A351_HANDLE();
	}
void UART02_IRQHandler(void){
		RS485_HANDLE();
	}

	
void RS485_HANDLE(){
		
		char u8InChar = 0xFF;
		uint32_t u32IntSts = UART0->ISR;

		if(u32IntSts & UART_ISR_RDA_INT_Msk)
		{

			/* Get all the input characters */
			while(UART_IS_RX_READY(UART0))
			{
				/* Get the character from UART Buffer */
				u8InChar = UART_READ(UART0);

				//send_string[j++]=u8InChar;
							printf("%c",u8InChar);
				

				/* Check if buffer full */
				
			}
			
		}

	
	}

void SYS_Init(void){
		/*---------------------------------------------------------------------------------------------------------*/
		/* Init System Clock                                                                                       */
		/*---------------------------------------------------------------------------------------------------------*/

		/* Enable Internal RC 22.1184MHz clock */
		CLK_EnableXtalRC(CLK_PWRCON_OSC22M_EN_Msk);

		/* Waiting for Internal RC clock ready */
		CLK_WaitClockReady(CLK_CLKSTATUS_OSC22M_STB_Msk);

		/* Switch HCLK clock source to Internal RC and HCLK source divide 1 */
		CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC, CLK_CLKDIV_HCLK(1));

		/* Enable external XTAL 12MHz clock */
		//CLK_EnableXtalRC(CLK_PWRCON_XTL12M_EN_Msk);
		/* Enable external 12MHz XTAL, internal 22.1184MHz */
    CLK_EnableXtalRC(CLK_PWRCON_XTL12M_EN_Msk | CLK_PWRCON_OSC22M_EN_Msk);
		
		 /* Enable PLL and Set PLL frequency */
    CLK_SetCoreClock(PLL_CLOCK);





		/* Waiting for external XTAL clock ready */
		//CLK_WaitClockReady(CLK_CLKSTATUS_XTL12M_STB_Msk);
		
    /* Waiting for clock ready */
    CLK_WaitClockReady(CLK_CLKSTATUS_PLL_STB_Msk | CLK_CLKSTATUS_XTL12M_STB_Msk | CLK_CLKSTATUS_OSC22M_STB_Msk);

		/* Switch HCLK clock source to PLL, STCLK to HCLK/2 */
    CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_PLL, CLK_CLKDIV_HCLK(2));

		/* Set core clock as PLL_CLOCK from PLL */
		CLK_SetCoreClock(PLL_CLOCK);
			CLK_SetSysTickClockSrc(CLK_CLKSEL0_STCLK_S_HCLK_DIV2);

		/* Enable UART module clock */
		CLK_EnableModuleClock(UART0_MODULE);
		CLK_EnableModuleClock(UART3_MODULE);
		CLK_EnableModuleClock(TMR0_MODULE);
		CLK_EnableModuleClock(TMR1_MODULE);
		CLK_EnableModuleClock(TMR2_MODULE);
		
		
		

		/* Select UART module clock source */
		CLK_SetModuleClock(UART0_MODULE, CLK_CLKSEL1_UART_S_HXT, CLK_CLKDIV_UART(1));
		CLK_SetModuleClock(UART3_MODULE, CLK_CLKSEL1_UART_S_HXT, CLK_CLKDIV_UART(1));
		CLK_SetModuleClock(TMR0_MODULE, CLK_CLKSEL1_TMR0_S_HCLK, 0);
		CLK_SetModuleClock(TMR1_MODULE, CLK_CLKSEL1_TMR1_S_HXT, 0);
		CLK_SetModuleClock(TMR1_MODULE, CLK_CLKSEL1_TMR2_S_HXT, 0);
			
		PllClock        = PLL_CLOCK;            // PLL
    SystemCoreClock = PLL_CLOCK / 1;        // HCLK
    CyclesPerUs     = PLL_CLOCK / 1000000;  // For SYS_SysTickDelay()


		/*---------------------------------------------------------------------------------------------------------*/
		/* Init I/O Multi-function                                                                                 */
		/*---------------------------------------------------------------------------------------------------------*/

		/* Set GPB multi-function pins for UART0 RXD(PB.0) and TXD(PB.1) */
		/* Set GPB multi-function pins for UART1 RXD(PB.4), TXD(PB.5), nRTS(PB.6) and nCTS(PB.7) */

		SYS->GPA_MFP &= ~(SYS_GPA_MFP_PA2_Msk | SYS_GPA_MFP_PA3_Msk);
		SYS->GPB_MFP &= ~(SYS_GPB_MFP_PB0_Msk | SYS_GPB_MFP_PB1_Msk);

		SYS->GPB_MFP |= (SYS_GPB_MFP_PB0_UART0_RXD | SYS_GPB_MFP_PB1_UART0_TXD
						  );
		SYS->GPA_MFP |= (
											SYS_GPA_MFP_PA3_UART3_RXD | SYS_GPA_MFP_PA2_UART3_TXD
						  );
		SYS->ALT_MFP4|=(SYS_ALT_MFP4_PA3_UART3_RXD| SYS_ALT_MFP4_PA2_UART3_TXD);
	}

unsigned char KEYS_PAD[4][4] ={'D', '#' , '0' , '*',
								   'C' ,'9' , '8', '7',
								   'B' ,'6' ,'5', '4',
								   'A' , '1', '2' , '3' };
//keypad 4x4
	
//setup scankey
void Init_Scankey(void){		
		GPIO_SetMode(PE, BIT5, GPIO_PMD_INPUT); 	//R1
		GPIO_SetMode(PA, BIT15, GPIO_PMD_INPUT);	//R2
		GPIO_SetMode(PA, BIT11, GPIO_PMD_INPUT);	//R3
		GPIO_SetMode(PA, BIT10, GPIO_PMD_INPUT);	//R4
	
		GPIO_SetMode(PB, BIT11, GPIO_PMD_OUTPUT); //C1
		GPIO_SetMode(PA, BIT14, GPIO_PMD_OUTPUT);	//C2
		GPIO_SetMode(PA, BIT13, GPIO_PMD_OUTPUT);	//C3
		GPIO_SetMode(PA, BIT12, GPIO_PMD_OUTPUT);	//C4
	
		GPIO_SetMode(PF, BIT4, GPIO_PMD_OUTPUT); //output vibrate
		PF4 = 1; 
		
		PB11 = 0;	//C1
		PA14 = 0;	//C2
		PA13 = 0;	//C3
		PA12 = 0;	//C4
}

//Init Colums of keypad
void colPin(int pin,int pinx){
	switch(pin)
	{
		case 0:
		{
			PB11 = pinx;
			break;
		}
		case 1:
		{
			PA14 = pinx;
			break;
		}
		case 2: 
		{
			PA13 = pinx;
			break;
		}
		case 3:
		{
			PA12 = pinx;
			break;
		}
	}
}	

//Init rows of keypad
int rowPin(int pin){
	switch(pin)
	{
		case 0:
		{
			if(PE5 == 0)
				return 0;
			else return 1;
			break;
		}
		case 1:
		{
			if(PA15 == 0)
				return 0;
			else return 1;
			break;
		}
		case 2: 
		{
			if(PA11 == 0)
				return 0;
			else return 1;
			break;
		}
		case 3:
		{
			if(PA10 == 0)
				return 0;
			else return 1;
			break;
		}
	}
	return 1;
}	

//Scankey function
unsigned char Scankey(){			
			unsigned int i =0, c =0,j=0;
			unsigned int u32DelayTime = 0;
			PE5 = 1;		//R1
			PA15 = 1;		//R2	
			PA11 = 1;		//R3
			PA10 = 1;		//R4
					
			PB11 = 0;	//C1
			PA14 = 0;	//C2
			PA13 = 0;	//C3
			PA12 = 0;	//C4
			TIMER_Delay(TIMER0, 3);
			if(PE5 == 0||PA15 == 0||PA11 == 0||PA10 == 0)
			{
				  TIMER_Delay(TIMER0, 1000);
					if(PE5 == 0||PA15 == 0||PA11 == 0||PA10 == 0)
				{
					for(i = 0;i<4;i++)
					{
						for(c = 0; c<4;c++)
						{
							colPin(c,1);
							if(i==c)
								colPin(c,0);
						}
						TIMER_Delay(TIMER0, 1000);
						for(j = 0;j<4;j++)
						{
							if(rowPin(j)==0)
							{
								TIMER_SET_CMP_VALUE(TIMER1, 0xFFFFFF);
								while(rowPin(j)==0);
								u32DelayTime = TIMER_GetCounter(TIMER1) / 1000;
								if(u32DelayTime < 60)
								return 'a';
							return KEYS_PAD[i][j];
						}
					}
				}
			}
		}
	return 'a';
}

void UART0_Init(){
		/*---------------------------------------------------------------------------------------------------------*/
		/* Init UART                                                                                               */
		/*---------------------------------------------------------------------------------------------------------*/
		/* Reset UART0 module */
		SYS_ResetModule(UART0_RST);

		/* Configure UART0 and set UART0 Baudrate */
		UART_Open(UART0, 9600);
	}
	
void UART3_Init(){
		/*---------------------------------------------------------------------------------------------------------*/
		/* Init UART                                                                                               */
		/*---------------------------------------------------------------------------------------------------------*/
		/* Reset UART0 module */
		SYS_ResetModule(UART3_RST);

		/* Configure UART0 and set UART0 Baudrate */
		UART_Open(UART3, 9600);
	}

	/*---------------------------------------------------------------------------------------------------------*/
	/* MAIN function                                                                                           */
	/*---------------------------------------------------------------------------------------------------------*/
	
	uint32_t count = 0;
	
int32_t main(void){

		/* Unlock protected registers */
		SYS_UnlockReg();

		/* Init System, peripheral clock and multi-function I/O */
		SYS_Init();

		/* Lock protected registers */
		SYS_LockReg();

		/* Init UART0 for printf */
		UART0_Init();
			UART3_Init();
		/* Init UART1 for testing */
	
			Init_Scankey();

			char key;
		
			TIMER1->TCSR = TIMER_PERIODIC_MODE | (12 - 1);
			TIMER_SET_CMP_VALUE(TIMER1, 0xFFFFFF);
			TIMER_Start(TIMER1);
			TIMER2->TCSR = TIMER_PERIODIC_MODE | (12 - 1);
			TIMER_SET_CMP_VALUE(TIMER2, 0xFFFFFF);
			TIMER_Start(TIMER2);
			send_ok = FALSE;
			UART_ENABLE_INT(UART3, (UART_IER_RDA_IEN_Msk | UART_IER_TOUT_IEN_Msk));
			NVIC_EnableIRQ(UART3_IRQn);
			UART3->FCR &=~0x00000100;
			
			sendATcmd("AT+RST\r\n");
			count=0;
			
			while(count<5){
				TIMER_Delay(TIMER0, 1000000);
				count++;
			}
			send_ok = TRUE;
				
			sendATcmd("AT+CWMODE=1\r\n");	
		
		 	while(send_ok);
		  send_ok = TRUE;
			
			sendATcmd("AT+CIPMUX=0\r\n");
			while(send_ok);
			send_ok = TRUE;
			
		//	sendATcmd("AT+CWJAP=\"Nhan_308\",\"nhantran97\"\r\n");
			sendATcmd("AT+CWJAP=\"NhanTran\",\"trongnhan\"\r\n");
			while(send_ok);
			send_ok = TRUE;
			
			sendATcmd("AT+CIPSTART=\"TCP\",\"35.220.215.145\",8444\r\n");
			//sendATcmd("AT+CIPSTART=\"TCP\",\"192.168.0.103\",8444\r\n");
			//sendATcmd("AT+CIPSTART=\"TCP\",\"192.168.43.157\",8444\r\n");
			while(send_ok);
			send_ok = TRUE;
			
			TIMER_SET_CMP_VALUE(TIMER2, 0xFFFFFF);
			
			while(1){
				key = Scankey();
				if(key == '2'||key == '4'||key == '6'||key == '8'||key=='5')
				{
					TIMER_SET_CMP_VALUE(TIMER2, 0xFFFFFF);
					if(key == '2')
						key ='2';
					if(key == '4')
						key ='4';
					if(key == '6')
						key ='6';
					if(key == '8')
						key ='8';
					if(key == '5')
						key ='5';
					sendATcmd("AT+CIPSEND=");
					sendATcmd("1\r\n");
					while(send_ok);
					send_ok = TRUE;
					sendATcmd(&key);
					while(send_ok);
					send_ok = TRUE;
				}
			}
	}